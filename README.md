I show that using the proper templates and data conditioning, the residual correlation following GW150914 claimed in Creswell, et al. (2017) vanishes.
